#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


pthread_mutex_t mutex;
struct coordenada
{
    int x;
    int y;
}typedef coordenada;

//Definicion de variables y estructuras para manejo lista de paquetes
struct {
    char * nombre;
    int telefono;
    char * direccion_text;
    char *estado;
    coordenada cordenada;
}typedef paquete;


//Definicion de estructuras para manejo de lista drones
struct{
    char * nom_drone;
    coordenada coord_drone;
    int tiempo_vuelo;
    
}typedef drone;

//variables globales de drones, paquetes e hilos
static drone *lista_drones;
static paquete *lista_paquetes;
static int tam_lista_paquetes;
static int finished=0;

pthread_t *lista_hilos;

//Declaracion de funciones del progama
void aleatorio_paquetes(int);
void aleatorio_drones(int);
double calcular_distancia(coordenada *paquete, coordenada *drone);
void entregar(drone *dron, paquete *paquete);
coordenada generarCordenada(int x, int y);
void *empezar_entrega(void *i);
void enlazar_hilo_drone(int numDrones);
void lock(pthread_mutex_t *mutex);
void unlock(pthread_mutex_t *mutex);
//Fin de declaracion de funciones del programa

//**********Metodo main***********//
int main(int argc, char *argv[]){
    if(argc!=3){
        fprintf(stderr,"Uso : n(Paquetes) m(drones) \n");
        exit(EXIT_FAILURE);
    }
  
    int num_paquetes=atoi(argv[1]);
    int num_drones=atoi(argv[2]);
    if(num_paquetes<1){
        fprintf(stderr,"Debe especificar un numero correcto de paquetes \n");
        exit(EXIT_FAILURE);
    }
    
    tam_lista_paquetes=num_paquetes;
    aleatorio_paquetes(num_paquetes);
    aleatorio_drones(num_drones);
    /**
     * se empiza con la entrega de paquetes utilizando hilos
    */
    pthread_mutex_init(&mutex,NULL);
    enlazar_hilo_drone(num_drones);
    printf("\t***Entregas Completadas***\n");
    pthread_mutex_destroy(&mutex);

    exit(EXIT_SUCCESS);

}


/**
 * Funcion: aleatorio_paquete
 * Generera una lista de paquetes y les da coordenas aleatoriamente
*/
void aleatorio_paquetes(int paquetes){
    
    lista_paquetes=(paquete*)malloc(paquetes*sizeof(paquete));
    for(size_t i = 0; i < paquetes; i++)
    {
        lista_paquetes[i].nombre=(char*)malloc(sizeof(char));
        lista_paquetes[i].direccion_text=(char*)malloc(sizeof(char));
        lista_paquetes[i].estado=(char*)malloc(sizeof(char));
        char *nombre=(char*)malloc(sizeof(char));
        char *direccion=(char*)malloc(sizeof(char));

        sprintf(nombre,"%d",i);
        strcpy(lista_paquetes[i].nombre,"Paquete_");
        strcat(lista_paquetes[i].nombre,nombre);
        lista_paquetes[i].telefono=i*2;
        strcpy(lista_paquetes[i].direccion_text,"Calle 23#12-13");
        strcpy(lista_paquetes[i].estado,"Sin_Entregar");
        lista_paquetes[i].cordenada=generarCordenada(0,0);
    }
}
/**
 * Se crea la lista de drones y se ubican aleatoriamente en el plano
*/
void aleatorio_drones(int drones){
    lista_drones=(drone *)malloc(drones*(sizeof(drone)));
    srand(time(NULL));
    char * nombre=(char*)malloc(sizeof(char));
    char *caracter=(char*)malloc(sizeof(char));
    strcpy(nombre,"Drone_");
    for(size_t i = 0; i < drones; i++)
    {
        lista_drones[i].nom_drone=(char*)malloc(sizeof(char));
        lista_drones[i].coord_drone=generarCordenada(0,0);
        sprintf(caracter,"%d",i);
        strcpy(lista_drones[i].nom_drone,nombre);
        strcat(lista_drones[i].nom_drone,caracter);   
    }
}

/**
 * Funcion entrega: realiza la entrega del paquete
 * actualiza la pocion del drone una vez entregado el paquete entregado
*/
void entregar(drone *dron, paquete *paquete){
    printf("\n\t%s en operacion, timpo de vuelo %d\n",dron->nom_drone,dron->tiempo_vuelo);
    sleep(dron->tiempo_vuelo); //Viajando...
    strcpy(paquete->estado,"Entregado");
    printf("\n\t%s entrego %s\n",dron->nom_drone,paquete->nombre);
    dron->coord_drone=paquete->cordenada;//Se actualiza la posicion del drone
}
/**
 * random de cordenadas, tanto para los drones, como los paquetes*/
coordenada generarCordenada(int x, int y){
    int point_x=rand()%100;
    int pont_y=rand()%100;
    coordenada cord;
    cord.x=point_x;
    cord.y=pont_y;
    return cord;
}
/**
 * Funcion pasada como parametro a un hilo, recibe como parametro un un drone
 * se mantiene ejecutando y realizando entregas con el dron pasado como parametro
 * finaliza cuando en la lista de paquetes no existan paquetes por entregar
*/
void * empezar_entrega(void * argv){
    drone *dron =((drone *) (argv));   
    while(!finished){
        int distancia=1000;
        int nueva_distancia=0;
        int pos=0;
        int flag=0;
        int timeVuelo; 
        int i;
        //Inicio de zona critica, puesto que pueden existir multiples drones tratando de calcular
        //el proximo paquete mas cercano, se hace necesario proteger el calculo de dicha distancia

        lock(&mutex); 
        for(i = 0; i < tam_lista_paquetes; i++)
        {
            //Si el paquete esta sin entregar se procede a calcular la distancia desde la ubicacion 
            //actual del drone hasta el paquete
            if(strcmp(lista_paquetes[i].estado,"Sin_Entregar")==0){
                nueva_distancia=(int)calcular_distancia(&dron->coord_drone,&lista_paquetes[i].cordenada);
                //Si la distancia apenas calculada es menor con la del anterior paquete
                //indica que el paquete al que se calculo distancia esta mas proximo que el anterior
                if(nueva_distancia<distancia){
                    distancia=nueva_distancia; //Se actualiza la nueva posicion
                    pos=i;
                    flag=1;
                }
            }
        }
        
        //Se cambia el esado del paquete, que es el mas proximo al drone
        strcpy(lista_paquetes[pos].estado,"Asignado");
        //Fin de zona critica, se desbloquea mutex
        unlock(&mutex);
        //Si al finalizar la iteracion del ciclo for flag=0 indica que no se encontraron mas
        //paquetes sin entregar, se finaliza ciclo while
        if(flag==0){
            printf("\t%s no encontro mas trabajo\n",dron->nom_drone);
            finished=0;
            break;
        }
    
        //Se calcula tiempo de vuelo
        timeVuelo=distancia%10;
        if(timeVuelo==0){
            timeVuelo=3;
        }
        dron->tiempo_vuelo=(timeVuelo+3);
        //Se empiza con el vuelo
        entregar(dron,&lista_paquetes[pos]);
  
    }
   
}
/**
 * Esta funcion se encarga de ligar cada dron de la empresa con un hilo de ejecucion
 * Una vez esta ligado el dron con el hilo se inicia su ejecucion
*/
void enlazar_hilo_drone(int numDrones){
    lista_hilos =(pthread_t *)malloc(numDrones*(sizeof(pthread_t)));
    int i=0;
    for( i = 0; i < numDrones; i++)
    {

        pthread_create(&lista_hilos[i],NULL,empezar_entrega,&lista_drones[i]);
    }
    for( i = 0; i < numDrones; i++)
    {
        pthread_join(lista_hilos[i],NULL);
    }
}
/**
 * Calcula la distancia euclidiana entre dos puntos en el plano xy
 * recibe como parametros las coordenadas de los dos puntos
 * y retorna la distancia
*/
double calcular_distancia(coordenada *drone, coordenada *paquete){
    double x1=(double)drone->x;
    double x2=(double)paquete->x;
    double y1=(double)drone->y;
    double y2=(double)paquete->y;
    double a=pow((x2-x1),2);
    double b=pow((y2-y1),2);
    double distancia=sqrt(a+b);
    return distancia;

}

/**
 * Funciones para bloquear y desbloquear los mutex ubicacion en zona critica
*/
void lock(pthread_mutex_t *mutex){
    pthread_mutex_lock(mutex);
}
void unlock(pthread_mutex_t *mutex){
    pthread_mutex_unlock(mutex);
}
